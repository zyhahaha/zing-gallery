module.exports = {
	cdnDomain: '/',
	title: 'Y & M',
	avatar: './img/avatar.png',
	wording: {
		noAccess: '抱歉，你没有权限访问'
	},
	albums: {
		"现在&上海": {
			description : "同居以后",
			name: "现在&上海"
		},
		"凤凰&长沙": {
			description : "白天普普通通的古镇，晚上让人难忘，酒吧、灯光，还有长沙的转折",
		}, 
		"大峡谷&天门山": {
		  description : "一起去的张家界 & 吓人的玻璃桥",
		},
		"小镇": {
		  description : "第一次带你去玩的地方",
		  name: "小镇"
		},
		// "私密": {
		// 	description: "这是一个需要密码的相册",
		//   name: "私密",
		//   password: "233",
		// 	passwordTips: "密码是233"
		// }
	}
}